package com.order.result.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.order.result.enums.OrderState;
import com.order.result.enums.Process;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "orders")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Order {
    @Id
    @GeneratedValue(generator = "seq_orders")
    private Long id;
    private String productName;
    private Integer amount;
    @Enumerated(EnumType.ORDINAL)
    private OrderState state;
    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderStep> orderSteps;

    public Order(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public OrderState getState() {
        return state;
    }

    public void setState(OrderState state) {
        this.state = state;
    }

    public List<OrderStep> getOrderSteps() {
        return orderSteps;
    }

    public void setOrderSteps(List<OrderStep> orderSteps) {
        this.orderSteps = orderSteps;
    }

    @PrePersist
    public void init(){
        this.state = OrderState.PENDING;
        OrderStep paymentStep = new OrderStep(Process.PAYMENT);
        OrderStep wareHouseStep = new OrderStep(Process.WAREHOUSE);
        this.orderSteps = new ArrayList<>(Arrays.asList(paymentStep, wareHouseStep));
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", amount=" + amount +
                ", state=" + state +
                ", orderSteps=" + orderSteps +
                '}';
    }

}
