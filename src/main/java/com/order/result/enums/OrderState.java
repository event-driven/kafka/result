package com.order.result.enums;

public enum OrderState {
    PENDING,
    PROCESSING,
    FAILED,
    SUCCESS;
}
