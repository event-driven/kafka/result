package com.order.result.enums;

public enum StepStatus {
    PENDING,
    PROCESSING,
    FAIL,
    SUCCESS;
}
