package com.order.result.listener;

import com.order.result.entity.Order;
import com.order.result.entity.OrderStep;
import com.order.result.enums.OrderState;
import com.order.result.enums.StepStatus;
import com.order.result.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;


@Component
public class KafkaResultListener {

    @Autowired
    private OrderRepository orderRepository;

    @Transactional
    @KafkaListener(topics = "result", containerFactory = "kafkaListenerContainerResultFactory")
    public void listener(String message) throws InterruptedException {
        Order order = orderRepository.getById(Long.parseLong(message));

        List<OrderStep> orderSteps = order.getOrderSteps();
        if(orderSteps.stream().anyMatch(o ->o.getStatus() == StepStatus.PROCESSING)){
            System.out.println(Instant.now().toString() + " --> skip check ");
            return;
        }
        if(orderSteps.stream().filter(o -> o.getStatus() == StepStatus.SUCCESS).count() == 2){
            order.setState(OrderState.SUCCESS);
        } else {
            order.setState(OrderState.FAILED);
        }
        Thread.sleep(10000l);
        orderRepository.save(order);
    }
}
